\context ChordNames
	\chords {
		\set majorSevenSymbol = "maj7"
		\set additionalPitchPrefix = "add"
		\set chordChanges = ##t

		% intro
		g1 g1

		% oh, sennor, muestrame el camino...
		g1 c1:9^7 g2 d2 c1
		g1 d1 c2 a2:m d1

		% señor, estoy cansado...
		g1 c1:9^7 g2 d2 c1
		e1:m b1:m c2 a2:m d2 c4 d4

		% porque en ti, señor...
		g1 b1:m c2 a2:m c2 d2

		% tómame, señor...
		g1 b1:m c1 c1:m
		g2 d2 c1

		% glue
		g1 g1

		% oh, sennor, mi alma te desea...
		g1 c1:9^7 g2 d2 c1
		g1 d1 c2 a2:m d1

		% señor, te doy mi vida...
		g1 c1:9^7 g2 d2 c1
		e1:m b1:m c2 a2:m d2 c4 d4

		% porque en ti, señor...
		g1 b1:m c2 a2:m c2 d2

		% tómame, señor...
		g1 b1:m c2 a2:m c2 d2

		% tómame, señor...
		e2:m b2:m c2 g2
		e2:m a2 c2 d2
		g1 b1:m c2 d2
		g1 b1:m c2 d2
		g1 g1
	}
