\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key g \major

		R1  |
		r2 r4 b, 8 c  |
		d 4 d 8 d d 4. e 8  |
		d 8 ( c ) c 2 b, 8 ( c )  |
%% 5
		d 4 d g fis  |
		e 2 ~ e 8 r b, c  |
		d 4 d d 4. d 8  |
		d 8 ( a, ) a, 4 ~ a, 8 r d d  |
		c 4 b, a, g,  |
%% 10
		a, 2 ~ a, 8 r g ( a )  |
		b 4 b b 4. c' 8  |
		b 8 ( g ) g 2 g 8 a  |
		b 4 b d' d'  |
		c' 2 ~ c' 8 r g ( a )  |
%% 15
		b 4 b c' 4. b 8  |
		b 8 ( fis ) fis 2 a 8 b  |
		c' 4 b a g  |
		a 4. r8 g 4 a  |
		b 4.. d 16 d 4 ~ d 16 r b b  |
%% 20
		b 8 a g fis fis 4 ~ fis 16 r a b  |
		c' 4.. e 16 e 4 ~ e 16 r c' c'  |
		c' 8 b a b c' 4 ( d' 8. ) r16  |
		d' 8 c' 16 b 8. c' 8 d' 4. r8  |
		d' 8 c' 16 b 8. c' 8 b g 4 r8  |
%% 25
		d' 8 c' 16 b 8. c' 8 d' 4 r8 d'  |
		d' 8 c' b c' b g 4 r8  |
		b 4 b a 4. b 8  |
		a 8 ( g 2.. )  |
		R1  |
%% 30
		r2 r4 b, 8 ( c )  |
		d 4 d 8 d d 4. e 8  |
		d 8 ( c ) c 2 b, 8 c  |
		d 4 d g fis  |
		e 2 ~ e 8 r b, c  |
%% 35
		d 4 d d 4. d 8  |
		d 8 ( a, ) a, 4 ~ a, 8 r d d  |
		c 4 b, a, g,  |
		a, 2 ~ a, 8 r g ( a )  |
		b 4 b b 4. c' 8  |
%% 40
		b 8 ( g ) g 2 g 8 a  |
		b 4 b d' d'  |
		c' 2 ~ c' 8 r g ( a )  |
		b 4 b c' 4. b 8  |
		b 8 ( fis ) fis 2 a 8 b  |
%% 45
		c' 4 b a g  |
		a 4. r8 g 4 a  |
		b 4.. d 16 d 4 ~ d 16 r b b  |
		b 8 a g fis fis 4 ~ fis 16 r a b  |
		c' 4.. e 16 e 4 ~ e 16 r c' c'  |
%% 50
		c' 8 b a b c' 4 ( d' 8. ) r16  |
		d' 8 c' 16 b 8. c' 8 d' 4. r8  |
		d' 8 c' 16 b 8. c' 8 b g 4 a 16 b  |
		c' 8 b a g 16 c' 8. b 8 a g  |
		g 4. ( a 8 ) a 4.. r16  |
%% 55
		b 8 b 16 b 8. e' 8 d' 4.. r16  |
		e' 8 d' 16 c' 8. b 8 c' b 8. r16 b b  |
		b 8. b c' 8 b 8. a b 8  |
		c' 2 b 4 a 8 r  |
		b 2. e' 4  |
%% 60
		e' 8 ( d' 2. ) r8  |
		R1  |
		b 2. e' 4  |
		e' 8 ( d' 2. ) r8  |
		c' 4 c' b a  |
%% 65
		g 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		Oh, Se -- ñor, mués -- tra -- "me el" ca -- mi -- no
		que de -- bo de  se -- guir, __
		i -- lu -- mí -- na -- "me el" sen -- de -- ro __
		que me lle -- va -- "rá has" -- ta ti. __

		Se -- ñor, es -- toy can -- sa -- do
		de bus -- car y "no en" -- con -- trar. __
		Se -- ñor, da -- me tu ma -- no,
		en ti quie -- ro des -- can -- sar.

		Por -- "que en" ti, Se -- ñor, __
		lo que "no ha" -- lla -- "ba en" -- con -- tré. __
		Por -- "que en" ti, Se -- ñor, __
		la ver -- dad yo pu -- de ver. __

		Tó -- ma -- me, Se -- ñor,
		llé -- va -- me con -- ti -- go,
		mués -- tra -- me "tu a" -- mor,
		sin ti "yo es" -- toy per -- di -- do.
		Tó -- ma -- me, Se -- ñor. __

		Se -- ñor, "mi al" -- ma te de -- se -- a,
		e -- lla tie -- ne sed de ti. __
		Yo mi co -- ra -- zón te a -- bro __
		pa -- ra que mo -- res en mí. __

		Se -- ñor, te doy mi vi -- da,
		haz lo que quie -- ras de mí. __
		Se -- ñor, es -- toy dis -- pues -- to:
		en ti yo quie -- ro vi -- vir.

		Por -- "que en" ti, Se -- ñor, __
		lo que "no ha" -- lla -- "ba en" -- con -- tré. __
		Por -- "que en" ti, Se -- ñor, __
		la ver -- dad yo pu -- de ver. __

		Tó -- ma -- me, Se -- ñor,
		llé -- va -- me con -- ti -- go,
		"a un" lu -- gar en don -- de pue -- da
		con -- tem -- plar -- te.

		Tó -- ma -- me, Se -- ñor,
		llé -- va -- me con -- ti -- go,
		no per -- mi -- tas que na -- da
		"me a" -- par -- te de... %ti.

		Tó -- ma -- me... __
		Tó -- ma -- me... __
		Tó -- ma -- me, Se -- ñor.
	}
>>
